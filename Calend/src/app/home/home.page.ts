import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';

import { Cars } from '../model/cars';
import { async } from 'q';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  
  cars: Cars[] = []
    
  constructor(public httpClient: HttpClient, private loadingCtrl: LoadingController){}

  async  onLoading(){
    const loading = await this.loadingCtrl.create({
      message: 'Espere um pouco..',
      spinner: 'lines-small'
      });
    return await loading.present()
                 .then(data => {
                   if(this.cars.length){
                     loading.dismiss();
                   }
                 });
  }
  
  ngOnInit(){
    this.onLoading();
    this.httpClient.get<Cars[]>('//localhost:8080/api/carro/listaTodos')
                    .subscribe(data => this.cars = data);   
  }

}
